<?php 

if (!function_exists('go')){
  function go($link){
    print '<meta http-equiv="refresh" content="1;URL='.$link.'">';
  }
}
if (!function_exists('goTemplate')){
  function goTemplate($link){
    if(is_file("template/".$link.".html")){
      include("template/".$link.".html");
    }else{
       include("template/error.html");
    }
  }
}
?>
